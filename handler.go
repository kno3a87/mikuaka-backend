package main

import (
    // 基本的なGo標準のnet/httpと比較してESTメソッド（GET、POST、PUTなど）に対応
    "github.com/julienschmidt/httprouter"
    "os"
    "strconv"

    "encoding/json"
    "database/sql"
    _ "github.com/go-sql-driver/mysql"
    "github.com/google/uuid"
    "fmt"
    "io/ioutil"
    "net/http"
)

func (j jsonTime) format() string {
    return j.Time.Format("2021-03-28 10:29:05")
}

func (j jsonTime) MarshalJSON() ([]byte, error) {
    return []byte(`"` + j.format() + `"`), nil
}

func Index(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
    fmt.Fprintf(w, "Hello world!")
}

type RegisterUserRequest struct {
    Name          string   `json:"name"`
    Password      string   `json:"password"`
}

type LoginRequest struct {
    Name          string   `json:"name"`
    Password      string   `json:"password"`
}

type YawarakasaRequest struct {
    Yawarakasa    string   `json:"yawarakasa"`
    Year          string   `json:"year"`
    Month         string   `json:"month`
    Day           string   `json:"day"`
}

func RegisterUser(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
    fmt.Print("新規登録だよ")
    // rで受け取った値をUnmarshalしてバイナリからGOのオブジェクト（json）にしてる
    defer r.Body.Close()
    body, _ := ioutil.ReadAll(r.Body)
    var req RegisterUserRequest
    json.Unmarshal(body, &req)
    // fmt.Print(req)     // 確認

    // DB準備
    // fmt.Print(os.Getenv("CLEARDB_DATABASE_URL"))    // 確認
    db, sqlErr := sql.Open("mysql", os.Getenv("CLEARDB_DATABASE_URL"))
    if sqlErr != nil {
      panic(sqlErr.Error())
    }
    defer db.Close() // 関数がリターンする直前に呼び出される

    // insert
    // userテーブル準備
    insertInfo, userErr := db.Prepare("INSERT INTO user(user_name, password, sex) VALUES(?,?,?)")
  	if userErr != nil {
  		panic(userErr.Error())
  	}
  	defer insertInfo.Close()
    // 値入れる
  	_, insertErr := insertInfo.Exec(req.Name, req.Password, "f")
    if insertErr != nil {
  		panic(insertErr.Error())
  	}

    // access_tokenテーブル準備
    insertToken, tokenErr := db.Prepare("INSERT INTO access_token(user_id, access_token) VALUES(?,?)")
  	if tokenErr != nil {
  		panic(tokenErr.Error())
  	}
  	defer insertToken.Close()
    // userのidをuserテーブルから取得
    // なぜか構造体作ってポインタとして取得しないとエラー
    var user User
    idErr := db.QueryRow("SELECT id FROM user WHERE user_name = ?", req.Name).Scan(&user.Id)
    if idErr != nil {
  		panic(idErr.Error())
  	}
    // UUID作成
    uuidObj, _ := uuid.NewUUID()
    fmt.Println("UUID:", uuidObj.String())
    // 値入れる
    insertToken.Exec(user.Id, uuidObj.String())

    // access_tokenをレスポンスとして返す
    // jsonの形にしてる
    fmt.Fprint(w, fmt.Sprintf("{\"access_token\": \"%s\"}", uuidObj.String()))
}

func Login(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
    // os.Stdoutは標準出力
    fmt.Fprint(os.Stdout, "ログインだよ")
    // rで受け取った値をUnmarshalしてバイナリからGOのオブジェクト（json）にしてる
    defer r.Body.Close()
    body, _ := ioutil.ReadAll(r.Body)
    var req LoginRequest
    json.Unmarshal(body, &req)
    fmt.Fprint(os.Stdout, req)   // 確認

    // DB準備
    // fmt.Print(os.Getenv("CLEARDB_DATABASE_URL"))    // 確認
    db, sqlErr := sql.Open("mysql", os.Getenv("CLEARDB_DATABASE_URL"))
    if sqlErr != nil {
      panic(sqlErr.Error())
    }
    defer db.Close() // 関数がリターンする直前に呼び出される

    // 受け取ったユーザ名とパスワードに一致しているID取得
    var user User
    loginErr := db.QueryRow("SELECT id FROM user WHERE user_name = ? and password = ?", req.Name, req.Password).Scan(&user.Id)
    if loginErr != nil {
      panic(loginErr.Error())
    }

    // IDがあればTokenにaccess_tokenをいれる
    // if user.Id != nil {
      idErr := db.QueryRow("SELECT access_token FROM access_token WHERE user_id = ?", user.Id).Scan(&user.Token)
      if idErr != nil {
        fmt.Fprint(os.Stdout, "たいへんだ！")
        panic(idErr.Error())
      }
      // access_tokenをレスポンスとして返す
      // jsonの形にしてる
      // fmt.Fprintは第一引数のところに第二引数を書き込むもの
      fmt.Fprint(w, fmt.Sprintf("{\"access_token\": \"%s\"}", user.Token))
    // }
}

func RegisterYawarakasa(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
    // os.Stdoutは標準出力
    fmt.Fprint(os.Stdout, "やわらかさ登録だよ")
    // rで受け取った値をUnmarshalしてバイナリからGOのオブジェクト（json）にしてる
    defer r.Body.Close()
    body, _ := ioutil.ReadAll(r.Body)
    var req YawarakasaRequest
    json.Unmarshal(body, &req)
    fmt.Fprint(os.Stdout, req)

    // headerからトークン読み取り
    var user User
    user.Token = r.Header.Get("x-access-token")
    fmt.Fprint(os.Stdout, user.Token)

    // DB準備
    // fmt.Print(os.Getenv("CLEARDB_DATABASE_URL"))    // 確認
    db, sqlErr := sql.Open("mysql", os.Getenv("CLEARDB_DATABASE_URL"))
    if sqlErr != nil {
      panic(sqlErr.Error())
    }
    defer db.Close() // 関数がリターンする直前に呼び出される

    // 受け取ったトークンのuser_id取得
    tokenErr := db.QueryRow("SELECT user_id FROM access_token WHERE access_token = ?", user.Token).Scan(&user.Id)
    if tokenErr != nil {
      panic(tokenErr.Error())
    }
    fmt.Fprint(os.Stdout, "ユーザIDは")
    fmt.Fprint(os.Stdout, user.Id)

    // user_idのyawarakasaテーブルにやわらかさ格納していく
    // userテーブル準備
    insertInfo, yawarakasaErr := db.Prepare("INSERT INTO yawarakasa(user_id, yawarakasa, year, month, day) VALUES(?,?,?,?,?)")
    if yawarakasaErr != nil {
      panic(yawarakasaErr.Error())
    }
    defer insertInfo.Close()
    // 受け取った値をTableに合わせてIntにする
    yawarakasa, _ := strconv.Atoi(req.Yawarakasa)
    year, _ := strconv.Atoi(req.Year)
    month, _ := strconv.Atoi(req.Month)
    day, _ := strconv.Atoi(req.Day)
    // 値入れる
    _, insertErr := insertInfo.Exec(user.Id, yawarakasa, year, month, day)
    if insertErr != nil {
      panic(insertErr.Error())
    }

    // 怒られるのでJSONなんか空で返す
    fmt.Fprint(w, fmt.Sprintf("{}"))
}

func GetYawarakasa(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
  // os.Stdoutは標準出力
  fmt.Fprint(os.Stdout, "やわらかさ見るよ")
  // rで受け取った値をUnmarshalしてバイナリからGOのオブジェクト（json）にしてる
  defer r.Body.Close()
  body, _ := ioutil.ReadAll(r.Body)
  var req YawarakasaRequest
  json.Unmarshal(body, &req)
  fmt.Fprint(os.Stdout, req)

  // headerからトークン読み取り
  var user User
  user.Token = r.Header.Get("x-access-token")
  fmt.Fprint(os.Stdout, user.Token)

  // DB準備
  // fmt.Print(os.Getenv("CLEARDB_DATABASE_URL"))    // 確認
  db, sqlErr := sql.Open("mysql", os.Getenv("CLEARDB_DATABASE_URL"))
  if sqlErr != nil {
    panic(sqlErr.Error())
  }
  defer db.Close() // 関数がリターンする直前に呼び出される

  // 受け取ったトークンのuser_id取得
  tokenErr := db.QueryRow("SELECT user_id FROM access_token WHERE access_token = ?", user.Token).Scan(&user.Id)
  if tokenErr != nil {
    panic(tokenErr.Error())
  }

  // user_idのyawarakasaテーブルから値をフロントに渡す
  var yawarakasa Yawarakasa
  rows, yawarakasaErr := db.Query("SELECT yawarakasa, year, month, day FROM yawarakasa WHERE user_id = ? ORDER BY year, month, day", user.Id)
  if yawarakasaErr != nil {
    panic(yawarakasaErr.Error())
  }
  defer rows.Close()

  fmt.Fprint(w, fmt.Sprintf("["))
  for rows.Next() {
    rowErr := rows.Scan(&yawarakasa.Yawarakasa, &yawarakasa.Year, &yawarakasa.Month, &yawarakasa.Day)
    if rowErr != nil {
      panic(rowErr.Error())
    }
    // やわらかさ返す
    fmt.Fprint(w, fmt.Sprintf("{\"yawarakasa\": \"%d\",", yawarakasa.Yawarakasa))
    fmt.Fprint(w, fmt.Sprintf("\"year\": \"%d\",", yawarakasa.Year))
    fmt.Fprint(w, fmt.Sprintf("\"month\": \"%02d\",", yawarakasa.Month))
    fmt.Fprint(w, fmt.Sprintf("\"day\": \"%02d\"},", yawarakasa.Day))
  }
  fmt.Fprint(w, fmt.Sprintf("]"))
}

func GetYawarakasaRanking(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
  // os.Stdoutは標準出力
  fmt.Fprint(os.Stdout, "やわらかさランキング見るよ")

  // DB準備
  // fmt.Print(os.Getenv("CLEARDB_DATABASE_URL"))    // 確認
  db, sqlErr := sql.Open("mysql", os.Getenv("CLEARDB_DATABASE_URL"))
  if sqlErr != nil {
    panic(sqlErr.Error())
  }
  defer db.Close() // 関数がリターンする直前に呼び出される

  // やわらかさテーブルのソートして5つユーザIDとやわらかさとってくる
  var user User
  rows, tokenErr := db.Query("SELECT user_id, MIN(yawarakasa) as yawarakasa FROM yawarakasa GROUP BY user_id ORDER BY yawarakasa asc limit 5")
  if tokenErr != nil {
    panic(tokenErr.Error())
  }
  fmt.Fprint(w, fmt.Sprintf("["))
  for rows.Next() {
    rowErr := rows.Scan(&user.Id, &user.Yawarakasa)
    if rowErr != nil {
      panic(rowErr.Error())
    }
    // user_idのuser_nameを取得
    yawarakasaErr := db.QueryRow("SELECT user_name FROM user WHERE id = ?", user.Id).Scan(&user.Name)
    if yawarakasaErr != nil {
      panic(yawarakasaErr.Error())
    }
    // ユーザネームとやわらかさ返す
    fmt.Fprint(w, fmt.Sprintf("{\"user_name\": \"%s\",", user.Name))
    fmt.Fprint(w, fmt.Sprintf("\"yawarakasa\": \"%d\"},", user.Yawarakasa))
  }
  fmt.Fprint(w, fmt.Sprintf("]"))
}

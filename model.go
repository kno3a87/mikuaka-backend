package main

import "time"

type jsonTime struct {
    time.Time
}

type User struct {
    Id           int       `json:"id"`
    Name         string    `json:"name"`
    Password     string    `json:"password"`
    Token        string    `json:"access_token"`
    Yawarakasa   int   `json:"yawarakasa"`
}

type Yawarakasa struct {
    Yawarakasa   int    `json:"yawarakasa"`
    Year         int    `json:"year"`
    Month        int    `json:"month"`
    Day          int    `json:"day"`
}

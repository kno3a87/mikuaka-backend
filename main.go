package main

import (
    "github.com/julienschmidt/httprouter"

    "net/http"
    "log"
    "fmt"
    "os"
)

func main() {
    fmt.Print("main.goだよ")
    router := httprouter.New()
    router.GET("/", Index)
    router.POST("/user", RegisterUser)
    router.POST("/user/login", Login)
    router.POST("/yawarakasa", RegisterYawarakasa)
    router.GET("/yawarakasa", GetYawarakasa)
    router.GET("/yawarakasa/ranking", GetYawarakasaRanking)

    log.Fatal(http.ListenAndServe(":"+os.Getenv("PORT"), router))

}
